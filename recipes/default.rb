#
# Cookbook Name:: gitlab-piwik
# Recipe:: default
#
# Copyright 2016, GitLab Inc.
#
# License: MIT
#
include_recipe 'gitlab-vault'

# Fetch secrets from Chef Vault
piwik_conf = GitLab::Vault.get(node, 'gitlab-piwik')

include_recipe 'chef_nginx'
template "#{node['nginx']['dir']}/sites-available/piwik" do
  source 'nginx-site-piwik.erb'
  owner  'root'
  group  'root'
  mode   '0644'
  variables(
      :fqdn => piwik_conf['fqdn'],
      :php_fpm_pass => piwik_conf['php_fpm_pass'],
      :piwik_install_path => piwik_conf['install_path']
  )
  notifies :restart, resources(:service => 'nginx')
end

nginx_site 'piwik' do
  enable true
end

%w(php-fpm php-cli php php-gd php-mysql php-geoip php-mbstring apache2-utils).each {|pkg| package pkg }

bash 'htpasswd file' do
  user 'root'
  code "htpasswd -bc /etc/nginx/.htpasswd admin #{piwik_conf['superuser']['password']}"
  not_if "htpasswd -bv /etc/nginx/.htpasswd admin #{piwik_conf['superuser']['password']}"
end

service 'php7.0-fpm' do
  action [:enable, :start]
end

template '/etc/php/7.0/fpm/php.ini' do
  source 'php.ini.erb'
  owner  'root'
  group  'root'
  mode   '0644'
  variables(piwik_conf)
  notifies :restart, resources(:service => 'php7.0-fpm'), :delayed
end

piwik_version = piwik_conf['version']

remote_file "#{Chef::Config[:file_cache_path]}/piwik-#{piwik_version}.tar.gz" do
  source "http://builds.piwik.org/piwik-#{piwik_version}.tar.gz"
  action :create_if_missing
end

directory piwik_conf['install_path'] do
  mode 0755
  owner 'www-data'
  action :create
end

bash 'install_piwik' do
  cwd Chef::Config[:file_cache_path]
  user 'root'
  code <<-EOH
    rm -rf #{piwik_conf['install_path']}/piwik
    tar zxf piwik-#{piwik_version}.tar.gz
    mv piwik #{piwik_conf['install_path']}
    chown -R www-data:www-data #{piwik_conf['install_path']}/piwik
    echo '#{piwik_version}' > #{piwik_conf['install_path']}/piwik/VERSION
  EOH
  not_if "test `cat #{piwik_conf['install_path']}/piwik/VERSION` = #{piwik_version}"
end

template "#{piwik_conf['install_path']}/piwik/config/config.ini.php" do
  source 'config.ini.php.erb'
  owner 'www-data'
  group 'www-data'
  mode 0644
  variables(piwik_conf)
  notifies :restart, resources(:service => 'php7.0-fpm'), :delayed
end

file "/etc/ssl/#{piwik_conf['fqdn']}.crt" do
  content piwik_conf['ssl_certificate']
  owner 'root'
  group 'root'
  mode 0644
  notifies :restart, resources(:service => 'nginx'), :delayed
end

file "/etc/ssl/#{piwik_conf['fqdn']}.key" do
  content piwik_conf['ssl_key']
  owner 'root'
  group 'root'
  mode 0600
  notifies :restart, resources(:service => 'nginx'), :delayed
end
