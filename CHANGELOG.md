gitlab-piwik CHANGELOG
========================

This file is used to list changes made in each version of the gitlab-piwik cookbook.

0.1.1
-----
- Jeroen - Use chef_nginx

0.1.0
-----
- Jeroen - Initial release of gitlab-piwik

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
