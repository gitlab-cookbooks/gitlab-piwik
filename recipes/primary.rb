include_recipe 'gitlab-piwik::default'

# Fetch secrets from Chef Vault
piwik_conf = GitLab::Vault.get(node, 'gitlab-piwik')

package 'mysql-server'

bash 'create mysql user' do
  user 'root'
  code <<-EOH
    mysql --defaults-file=/etc/mysql/debian.cnf -e "CREATE USER '#{piwik_conf['database']['username']}'@'%' IDENTIFIED BY '#{piwik_conf['database']['password']}';"
    mysql --defaults-file=/etc/mysql/debian.cnf -e "GRANT ALL ON #{piwik_conf['database']['dbname']}.* TO '#{piwik_conf['database']['username']}'@'%';"
  EOH
  not_if "mysql --defaults-file=/etc/mysql/debian.cnf -e 'use mysql;select user from user where user = \"#{piwik_conf['database']['username']}\"' | grep #{piwik_conf['database']['username']}"
end

cron 'hourly_piwik_archive' do
  minute '12'
  user 'www-data'
  mailto piwik_conf['superuser']['email'] || 'root'
  command "/usr/bin/php5 #{piwik_conf['install_path']}/piwik/console core:archive --url=https://#{piwik_conf['fqdn']}/ > /dev/null"
end
