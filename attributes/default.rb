default['gitlab-piwik']['version'] = '2.16.2'
default['gitlab-piwik']['fqdn'] = node['fqdn']
default['gitlab-piwik']['install_path'] = '/var/www'
default['gitlab-piwik']['php_fpm_memory_limit'] = '128M'
default['gitlab-piwik']['php_fpm_max_execution_time'] = '30'
default['gitlab-piwik']['php_fpm_pass'] = 'unix:/var/run/php/php7.0-fpm.sock'
default['gitlab-piwik']['ssl_certificate'] = 'override_attribute in role!'
default['gitlab-piwik']['ssl_key'] = 'override_attribute in vault!'
default['gitlab-piwik']['salt'] = 'override_attribute in vault!'
default['gitlab-piwik']['trusted_hosts'] = []

# Params used in secondary recipe only:
default['gitlab-piwik']['database']['host'] = '127.0.0.1'
default['gitlab-piwik']['database']['port'] = '3306'
default['gitlab-piwik']['database']['username'] = 'piwik'
default['gitlab-piwik']['database']['password'] = 'override_attribute in vault!'
default['gitlab-piwik']['database']['dbname'] = 'piwik'
default['gitlab-piwik']['database']['tables_prefix'] = 'piwik_'

default['nginx']['default_site_enabled'] = false
